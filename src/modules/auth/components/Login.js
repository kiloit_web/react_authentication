import loginImage from "../../../assets/image/login.avif";
import "../../../assets/css/main.css";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const usenavigate = useNavigate();

  const ProceedLogin = (e) => {
    e.preventDefault();
    if (validate()) {
      //implementation
      // console.log("proceed");
      fetch("https://dummyjson.com/auth/login/")
        .then((res) => {
          return res.json();
        })
        .then((resp) => {
          // console.log(resp);
          if (Object.keys(resp).length === 0) {
            console.log("Please Enter valid username");
          } else {
            if (resp.password === password) {
              console.log("Success!");
              usenavigate("/");
            } else {
              console.log("Please Enter valid credentials");
            }
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  const validate = () => {
    let result = true;
    if (username === "" || username === null) {
      result = false;
    }
    if (password === "" || password === null) {
      result = false;
    }
    return result;
  };
  return (
    <>
      <h1 style={{ textAlign: "center" }}>Login Form</h1>
      <div className="loginMain">
        <div className="login">
          <div className="image">
            <img src={loginImage} alt="login_image" />
          </div>
          <form onSubmit={ProceedLogin}>
            <label>Username</label> <br />
            <input
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              placeholder="Username"
            />{" "}
            <br />
            <label>Password</label> <br />
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              placeholder="Password"
            />{" "}
            <br />
            <button type="submit">Click Me!</button>
          </form>
        </div>
      </div>
    </>
  );
};

export default Login;
