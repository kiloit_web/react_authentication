import { Route,BrowserRouter, Routes } from "react-router-dom";
import Layout from "./modules/layout/Index";
import HomePage from "./modules/home/HomePage";
import BlogPage from "./modules/blog/BlogPage";
import ContactPage from "./modules/contact/ContactPage";
import ErrorPage from "./modules/error/ErrorPage";
import Login from "./modules/auth/components/Login";

function App() {
  return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
              <Route index element={<HomePage />}/>
              <Route path="blog" element={<BlogPage />}/>
              <Route path="contact" element={<ContactPage />}/>
              <Route path="login" element={<Login/>}/>
              <Route path="*" element={<ErrorPage />}/>
          </Route>
        </Routes>
      </BrowserRouter>
  );
}

export default App;


